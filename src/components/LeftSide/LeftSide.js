import React, {Component} from 'react';
import Robot from '../Robot/Robot';

class LeftSide extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return this.props.robots.map((data, index) => {
            return (
                <Robot key={index} robot={data} />
            );
        });
    }
}

export default LeftSide;

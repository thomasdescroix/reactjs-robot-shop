import React, { Component } from 'react';

class Visual extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let render_visual;
        switch (this.props.type) {
            case 'img':
                render_visual=(
                    <img src={this.props.src} alt="imgCard" className='img-fluid'/>
                );
            break;
            case 'video':
                render_visual=(
                    <iframe title={this.props.src} src={this.props.src} />
                );
            break;
        }
        return render_visual;
    }
}

export default Visual;
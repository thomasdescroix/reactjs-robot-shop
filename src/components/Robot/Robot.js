import React, { Component } from 'react';
import Label from './containers/Label';
import Visual from './containers/Visual';

class Robot extends  Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="card">
                <div className="card-header">
                    <h3 className="card-title">
                        Robot description
                    </h3>
                </div>
                <div className="card-body">
                    <Label
                        title={this.props.robot.title}
                        id={this.props.robot.id}
                    />
                    <Visual
                        type={this.props.robot.visual_type}
                        src={this.props.robot.visual_src}
                    />
                </div>
            </div>
        );
    }
}

export default Robot;
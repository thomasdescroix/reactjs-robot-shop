import React, {Component} from 'react';
import './lib/bootstrap/css/bootstrap.min.css';
import * as jsonSource from './sources/robots';

import LeftSide from './components/LeftSide/LeftSide';

class App extends Component {
    constructor(props) {
        super(props);
        let robot_list;
        this.state = { robot_list:jsonSource.robots };
        console.log(this.state.robot_list);
    }
    render() {
        return (
            <div className="container">
                <h1>Welcome to Robot Shop</h1>
                <div className="row">
                    <div className="col-4">
                        <LeftSide robots={this.state.robot_list}/>
                    </div>
                    <div className="col-4"></div>
                    <div className="col-4"></div>
                </div>
            </div>
        );
    }
}

export default App;
